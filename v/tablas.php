<?php
	$mensaje = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est quaerat nesciunt facere, nisi id corrupti deserunt enim pariatur nobis numquam, velit.';
	$titulo = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?= $title ?></title>
</head>
<body>
<span>
	<label style="text-align:center;background-color: #0aa699;color: #ffffff;font-size:25px;border-radius:20px">
		<?= $mensaje ?>
	</label>
</span>
<table width="100%" cellspacing="0" cellpadding="2" style="border-color:black">
	<tr>
		<td align="center" style="background-color: #18B5F0" colspan = "2">
			<b><?= $titulo ?></b>
		</td>
	</tr>
</table>
<table width="100%" cellspacing="0" cellpadding="2" style="border-color:black">
	<thead style="background-color: #d6e0f5; color: #000;">
		<tr>
			<th>Lorem</th>
			<th>Lorem</th>
			<th>Lorem</th>
			<th>Lorem</th>
			<th>Lunes</th>
			<th>Martes</th>
			<th>Miercoles</th>
			<th>Jueves</th>
			<th>Viernes</th>
			<th>Sabado</th>
			<th>Domingo</th>
			<th>Lorem</th>
			<th>Lorem</th>
			<th>Lorem</th>
			<th>Lorem ipsum dolor</th>
			<th>Lorem ipsum dolor</th>
		</tr>
	</thead>
	<tbody>
<?php
		for ($i=0; $i < 10; $i++) { 
?>
		<tr>
			<td>Lorem</td>
			<td>Lorem</td>
			<td>123</td>
			<td>Nombre Apellido</td>
			<td>A</td>
			<td>A</td>
			<td>A</td>
			<td>A</td>
			<td>A</td>
			<td>A</td>
			<td>A</td>
			<td>0.00</td>
			<td>0.00</td>
			<td>0.00</td>
			<td></td>
			<td></td>
		<tr>
<?php			
		}
?>	
	</tbody>
</table>

<br>
<hr>
<br>

<div style="background-color: #000; color:#fff; text-align:left; font-size:8px; font-family: calibri; padding: 5px 20px; border-radius: 5px 5px 0px 0px">
	<label>
		<h1 style="padding: 0px;"><?= $titulo ?></h1>
	</label>
</div>

<table width="100%" cellspacing="1" cellpadding="10" style="border-color:black; font-family: calibri; border:1px solid #dddddd;">
	<caption>
		<div style="background-color: #a5ecb1;color:#266d32;font-family: calibri;font-size:16px;padding: 15px;text-align:left;">
			<label>
				<?= $mensaje ?>
			</label>
		</div>
	</caption>
	<thead>
		<tr style="background-color: #ccc;">
			<th style="text-align: left;">Lorem</th>
			<th style="text-align: left;">Lorem</th>
			<th style="text-align: left;">Lorem</th>
			<th style="text-align: left;">Lorem</th>
			<th style="text-align: center;">Lunes</th>
			<th style="text-align: center;">Martes</th>
			<th style="text-align: center;">Miercoles</th>
			<th style="text-align: center;">Jueves</th>
			<th style="text-align: center;">Viernes</th>
			<th style="text-align: center;">Sabado</th>
			<th style="text-align: center;">Domingo</th>
			<th style="text-align: left;">Lorem</th>
			<th style="text-align: left;">Lorem</th>
			<th style="text-align: left;">Lorem</th>
			<th style="text-align: left;">Lorem ipsum dolor</th>
			<th style="text-align: left;">Lorem ipsum dolor</th>
		</tr>
	</thead>
	<tbody>
<?php
		for ($i=0; $i < 10; $i++) { 
			if ($i%2 == 1) {

			echo '<tr style="background-color: #f2f2f2;">';

			}
			else {

			echo '<tr style="background-color: #fffff;">';

			}
?>

			<td style="text-align: left;">Lorem</td>
			<td style="text-align: left;">Lorem</td>
			<td style="text-align: left;">123</td>
			<td style="text-align: left;">Nombre Apellido</td>
			<td style="text-align: center;">A</td>
			<td style="text-align: center;">A</td>
			<td style="text-align: center;">A</td>
			<td style="text-align: center;">A</td>
			<td style="text-align: center;">A</td>
			<td style="text-align: center;">A</td>
			<td style="text-align: center;">A</td>
			<td style="text-align: left;">0.00</td>
			<td style="text-align: left;">0.00</td>
			<td style="text-align: left;">0.00</td>
			<td style="text-align: left;"></td>
			<td style="text-align: left;"></td>
		<tr>
<?php
		}
?>	
	</body>
</html>