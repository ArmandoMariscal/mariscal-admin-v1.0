<?php

session_start();

include('../conexion.php'); 
include('../model.php'); 

?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="bootstrap-grid.min.css">
	<link rel="stylesheet" type="text/css" href="bootstrap-reboot.min.css">
</head>
<body>
	<br>
	<div class="container">
		<h3>Subir Archivos</h3>
		<form action="" method="post" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-12">
					<label for="archivos"></label>
					<input type="file" class="form-control" name="archivos" id="archivos" value="" />
					<br><br>
					<label for="titulo">Título</label>
					<input type="text"  class="form-control" name="titulo" id="titulo" value="" />
					<br><br>
					<input type="submit" class="btn btn-md btn-success" style="width:100%" value="Subir Archivo">
				</div>
			</div>
			
		</form>
		
	</div>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>